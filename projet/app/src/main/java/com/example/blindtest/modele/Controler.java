package com.example.blindtest.modele;

import com.example.blindtest.modele.jeux.Classique;

import java.io.Serializable;

public class Controler implements Serializable {


    private Galerie galerie;

    private transient MusiqueLoader loader;

    private Jeu jeu;

    public Galerie getGalerie() {
        return galerie;
    }

    public MusiqueLoader getLoader() {
        if(loader==null){
            loader = new MusiqueLoader();
        }
        return loader;
    }

    public Jeu getJeu() {
        return jeu;
    }

    public Controler(String nomGalerie){
        galerie= new Galerie(nomGalerie);
    }

    public boolean initialiserJeu(boolean allMusics){
        if (galerie.getMusiques().size()==0) return false;
        boolean res = false;
        jeu = new Classique();
        if(allMusics) {
            jeu.addMusiques(galerie.getMusiques());
            res=true;
        }
        else{
            for (PlayList p : galerie.getPlaylists()) {
                if (p.isSelected() && p.getMusiques().size()!=0){
                    res=true;
                    jeu.addMusiques(p.getMusiques());
                }
            }
        }
        if (jeu.getMusiques().size()<2) return false;
        return res;
    }

}
