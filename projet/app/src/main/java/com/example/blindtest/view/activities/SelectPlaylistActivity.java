package com.example.blindtest.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.PlaylistAdaptateur;
import com.example.blindtest.modele.Controler;

public class SelectPlaylistActivity extends AppCompatActivity {

    private Controler controler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_playlist);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        controler =(Controler) getIntent().getSerializableExtra("CONTROLER");
        controler.getGalerie().deselectAll();
        ListView listView = findViewById(R.id.list_playlists);
        listView.setAdapter(new PlaylistAdaptateur(this,android.R.layout.simple_list_item_1, controler.getGalerie().getPlaylists(), true));
    }

    public void clickPlay(View view){
        if (controler.initialiserJeu(false)){
            Intent intent = new Intent(this, GameActivity.class);
            intent.putExtra("CONTROLER", controler);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.select_playlist_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_use_all_music:
                if (controler.initialiserJeu(true)){
                    Intent intent = new Intent(this, GameActivity.class);
                    intent.putExtra("CONTROLER", controler);
                    startActivity(intent);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
