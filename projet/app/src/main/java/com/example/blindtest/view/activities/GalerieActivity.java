package com.example.blindtest.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.blindtest.R;
import com.example.blindtest.modele.Controler;

public class GalerieActivity extends AppCompatActivity {
    private Controler controler;

    private static final int REQUEST_MUSIC = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controler = (Controler) getIntent().getSerializableExtra("CONTROLER");
        setContentView(R.layout.galerie);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        setResult(RESULT_OK,getIntent());
    }

    public void clickViewMusics (View view) {
        Intent intent = new Intent(this, MusiqueActivity.class);
        intent.putExtra("CONTROLER", controler);
        intent.putExtra("PLAYLIST", -1);
        startActivityForResult(intent, REQUEST_MUSIC);
    }

    public void clickViewPlaylists(View view){
        Intent intent = new Intent(this, ListPlaylistActivity.class);
        intent.putExtra("CONTROLER", controler);
        startActivityForResult(intent, REQUEST_MUSIC);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            if (requestCode == REQUEST_MUSIC) {
                controler = (Controler)data.getSerializableExtra("CONTROLER");
                getIntent().putExtra("CONTROLER", controler);
            }
        }
    }
}
