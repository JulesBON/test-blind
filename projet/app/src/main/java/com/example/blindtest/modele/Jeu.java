package com.example.blindtest.modele;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class Jeu implements Serializable {
    private List<Musique> musiques = new ArrayList<>();

    public List<Musique> getMusiques() {
        return musiques;
    }

    private int score = 0;

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        this.score += score;
    }

    public void addMusique(Musique musique) {
        if(!musiques.contains(musique))
            musiques.add(musique);
    }

    public void addMusiques(List<Musique> musiques) {
        for(Musique m: musiques){
            addMusique(m);
        }
    }
}
