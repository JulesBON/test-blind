package com.example.blindtest.view.adaptateurs;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.blindtest.modele.Musique;

import java.util.List;

public class MusiqueAdaptateur extends ArrayAdapter<Musique> {

    public MusiqueAdaptateur(@NonNull Context context, int resource, @NonNull List<Musique> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Musique musique = getItem(position);
        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.VERTICAL);
        TextView name = new TextView(getContext());
        name.setText("Musique : "+musique.getName());
        layout.addView(name);
        return layout;
    }
}
