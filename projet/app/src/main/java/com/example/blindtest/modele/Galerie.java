package com.example.blindtest.modele;

import java.util.ArrayList;
import java.util.List;

public class Galerie extends PlayList{

    List<PlayList> playlists = new ArrayList<>();

    public List<PlayList> getPlaylists() {
        return playlists;
    }

    public void addPlaylist(PlayList playlist) {
        if(!playlists.contains(playlist))
            playlists.add(playlist);
    }

    public void removePlaylist(int index){
        playlists.remove(index);
    }

    public boolean renamePlaylist(PlayList playList, String name){
        if(playList.getName().equals(name))return true;
        for (PlayList p : playlists){
            if(p.getName().equals(name)) return false;
        }
        playList.setName(name);
        return true;
    }

    public Galerie(String name) {
        super(name);
    }

    @Override
    public void removeMusique(Musique musique){
        for(PlayList p : playlists){
            p.removeMusique(musique);
        }
        super.removeMusique(musique);
    }

    public void deselectAll(){
        for(PlayList p: playlists){
            p.setSelected(false);
        }
    }
}

