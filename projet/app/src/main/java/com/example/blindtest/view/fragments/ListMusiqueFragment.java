package com.example.blindtest.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;

import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.MusiqueAdaptateur;
import com.example.blindtest.view.activities.DetailMusiqueActivity;
import com.example.blindtest.view.activities.MusiqueActivity;

public class ListMusiqueFragment extends ListFragment {
    boolean dualPane;
    int curCheckPosition = 0;

    private static final int REQUEST_DETAIL = 1;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(((MusiqueActivity)getActivity()).getPlaylist()>=0){
            setListAdapter(new MusiqueAdaptateur(getActivity(),
                    android.R.layout.simple_list_item_activated_1, ((MusiqueActivity)getActivity()).getControler().getGalerie().getPlaylists().get(((MusiqueActivity)getActivity()).getPlaylist()).getMusiques()));
        }
        else{
            setListAdapter(new MusiqueAdaptateur(getActivity(),
                    android.R.layout.simple_list_item_activated_1, ((MusiqueActivity)getActivity()).getControler().getGalerie().getMusiques()));
        }

        View detailsFrame = getActivity().findViewById(R.id.details_galerie);
        dualPane = detailsFrame != null && detailsFrame.getVisibility() == View.VISIBLE;

        if (savedInstanceState != null) {
            curCheckPosition = savedInstanceState.getInt("curChoice", 0);
        }

        if (dualPane) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            showDetails(curCheckPosition);
        }

    }

    public void resetPosition(){
        curCheckPosition=0;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("curChoice", curCheckPosition);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        showDetails(position);
    }

    public void showDetails(int index) {
        curCheckPosition = index;

        if (dualPane) {

            getListView().setItemChecked(index, true);

            DetailMusiqueFragment details = DetailMusiqueFragment.newInstance(index, ((MusiqueActivity)getActivity()).getControler(), ((MusiqueActivity)getActivity()).getPlaylist());

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.details_galerie, details);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.commit();

        } else {
            Intent intent = new Intent(getActivity(), DetailMusiqueActivity.class);
            intent.putExtra("INDEX", index);
            intent.putExtra("CONTROLER", ((MusiqueActivity)getActivity()).getControler());
            intent.putExtra("PLAYLIST", ((MusiqueActivity)getActivity()).getPlaylist());
            getActivity().startActivityForResult(intent, REQUEST_DETAIL);
        }
    }

}
