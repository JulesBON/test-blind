package com.example.blindtest.view.fragments;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.MusiqueAdaptateur;
import com.example.blindtest.view.adaptateurs.PlaylistAdaptateur;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.modele.Musique;
import com.example.blindtest.modele.PlayList;
import com.example.blindtest.view.activities.DetailMusiqueActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class DetailMusiqueFragment extends Fragment {

    private Controler controler;
    private MediaPlayer mediaPlayer = new MediaPlayer();
    private boolean isPlaying = false;


    public static DetailMusiqueFragment newInstance(int index, Controler controler, int playlist) {
        DetailMusiqueFragment f = new DetailMusiqueFragment();

        Bundle args = new Bundle();
        args.putInt("INDEX", index);
        args.putSerializable("CONTROLER", controler);
        args.putInt("PLAYLIST", playlist);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onStop() {
        super.onStop();
        mediaPlayer.stop();
        if(getActivity().getClass()== DetailMusiqueActivity.class) {
            File fichier = new File(getActivity().getFilesDir() + "/save.bin");
            try {
                FileOutputStream fo = new FileOutputStream(fichier);
                ObjectOutputStream os = new ObjectOutputStream(fo);
                os.writeObject(controler);
                os.close();
            } catch (FileNotFoundException e) {
                return;
            } catch (IOException e) {
                return;
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        controler = (Controler)getArguments().getSerializable("CONTROLER");

        final PlayList playlist;
        int argPlaylist = getArguments().getInt("PLAYLIST", -1);
        if (argPlaylist>=0){
            playlist=controler.getGalerie().getPlaylists().get(argPlaylist);
        }
        else{
            playlist=controler.getGalerie();
        }

        if (playlist.getMusiques().size()==0) return null;

        View view = inflater.inflate(R.layout.musique_detail, container, false);

        final Fragment frag = getActivity().getSupportFragmentManager().getFragments().get(0);
        final boolean isLandscape=frag.getClass() == ListMusiqueFragment.class;

        final int index = getArguments().getInt("INDEX", 0);

        final Musique musique;
        musique = playlist.getMusiques().get(index);

        final Intent res = new Intent();
        res.putExtra("CONTROLER", controler);
        res.putExtra("SUPPR", false);
        getActivity().setResult(Activity.RESULT_OK, res);


        EditText name = view.findViewById(R.id.name);
        final TextView seekValue = view.findViewById(R.id.seekValue);
        Button supprButton = view.findViewById(R.id.supprButton);
        final Button playButton = view.findViewById(R.id.playButton);
        Button addtoplaylistButton = view.findViewById(R.id.addtoplaylistbutton);
        SeekBar debutSeekBar = view.findViewById(R.id.debutSeekBar);
        name.setText(musique.getName());

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                musique.setName(s.toString());
            }
        });


        supprButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playlist.removeMusique(musique);
                if(isLandscape){
                    ((MusiqueAdaptateur)((ListMusiqueFragment)frag).getListView().getAdapter()).notifyDataSetChanged();
                    ((ListMusiqueFragment)frag).showDetails(0);
                }
                else{
                    res.putExtra("SUPPR", true);
                    getActivity().finish();
                }
            }

        });
        seekValue.setText(Integer.toString(musique.getDebut()/60000)+"min:"+Integer.toString((musique.getDebut()/1000)%60)+"s");

        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                try {
                    mediaPlayer.stop();
                    mediaPlayer.prepare();
                    isPlaying = false;
                    playButton.setText(getString(R.string.playDetail));
                }
                catch(IOException e){

                }
            }
        });

        try{
            mediaPlayer.setDataSource(musique.getPath());
            mediaPlayer.prepare();
            debutSeekBar.setMax(mediaPlayer.getDuration());
            debutSeekBar.setProgress(musique.getDebut());

            playButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try{
                        if(isPlaying){
                            mediaPlayer.stop();
                            mediaPlayer.prepare();
                            isPlaying=false;
                            playButton.setText(getString(R.string.playDetail));
                        }
                        else{
                            mediaPlayer.seekTo(musique.getDebut());
                            mediaPlayer.start();
                            isPlaying=true;
                            playButton.setText(getString(R.string.stopDetail));
                        }
                    }
                    catch(IOException e){

                    }

                }
            });
        }
        catch(IOException e){

        }
        debutSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                seekValue.setText(Integer.toString(progress/60000)+"min:"+Integer.toString((progress/1000)%60)+"s");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                musique.setDebut(seekBar.getProgress());
            }
        });

        addtoplaylistButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListView listView = new ListView(getContext());
                listView.setAdapter(new PlaylistAdaptateur(getContext(),android.R.layout.simple_list_item_1, controler.getGalerie().getPlaylists(),false));
                AlertDialog.Builder popupbuilder = new AlertDialog.Builder(getContext());
                final AlertDialog popup = popupbuilder.create();
                popup.setView(listView);
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        controler.getGalerie().getPlaylists().get(position).addMusique(musique);
                        popup.dismiss();
                    }
                });
                popup.show();
            }
        });

        return view;
    }



}
