package com.example.blindtest.view.activities;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.blindtest.R;
import com.example.blindtest.modele.bluetooth.AcceptServer;
import com.example.blindtest.modele.bluetooth.ConnectThread;
import com.example.blindtest.modele.bluetooth.MyBluetoothService;

import java.util.Set;

import static java.lang.System.exit;

public class ParameterActivity extends AppCompatActivity {
    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> pairedDevices;
    private final static int REQUEST_ENABLE_BT = 1;
    MyBluetoothService btService = new MyBluetoothService();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.parametres);
        super.onCreate(savedInstanceState);
    }

    public void bluetooth(View view){
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            exit(1);
        }
        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        pairedDevices = bluetoothAdapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                Log.d("JULES", deviceName);
                Log.d("JULES", deviceHardwareAddress);
            }
        }
    }

    public void send(View view) {
        AcceptServer acceptServer = new AcceptServer();
        acceptServer.start();
    }

    public void recv(View view) {
        Set<BluetoothDevice> pairedDevices = this.pairedDevices;
        Log.d("JULES", "Oui");

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            ConnectThread connectThread;
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                if (deviceName == "Jules") {
                    connectThread = new ConnectThread(device);
                    connectThread.start();
                }
            }
        }

    }
}
