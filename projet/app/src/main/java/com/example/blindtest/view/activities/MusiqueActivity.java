package com.example.blindtest.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;

import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.MusiqueAdaptateur;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.view.fragments.ListMusiqueFragment;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class MusiqueActivity extends AppCompatActivity {

    private  Controler controler;

    private int playlist;

    public Controler getControler() {
        return controler;
    }

    public int getPlaylist() {
        return playlist;
    }

    private static final int REQUEST_DETAIL = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.playlist);
        controler =(Controler) getIntent().getSerializableExtra("CONTROLER");
        playlist = getIntent().getIntExtra("PLAYLIST", -1);
        Toolbar myToolbar =findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        if(playlist>=0){
            getSupportActionBar().setTitle(controler.getGalerie().getPlaylists().get(playlist).getName());
        }
        else
        {
            getSupportActionBar().setTitle(controler.getGalerie().getName());
        }
        setResult(RESULT_OK,getIntent());

    }

    @Override
    protected void onStop() {
        super.onStop();
        File fichier = new File(getFilesDir()+"/save.bin");
        try{
            FileOutputStream fo =new FileOutputStream(fichier);
            ObjectOutputStream os = new ObjectOutputStream(fo);
            os.writeObject(controler);
            os.close();
        }
        catch(FileNotFoundException e){
            return;
        }
        catch(IOException e){
            return;
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            if (requestCode == REQUEST_DETAIL) {
                controler = (Controler)data.getSerializableExtra("CONTROLER");
                Intent intent = getIntent();
                intent.putExtra("CONTROLER", controler);
                setResult(RESULT_OK, intent);
                FragmentManager manager = getSupportFragmentManager();
                ListMusiqueFragment lfrag =((ListMusiqueFragment)manager.findFragmentById(R.id.list_galerie));
                if(playlist>=0){
                    lfrag.getListView().setAdapter(new MusiqueAdaptateur(this, android.R.layout.simple_list_item_activated_1, controler.getGalerie().getPlaylists().get(playlist).getMusiques()));
                }
                else{
                    lfrag.getListView().setAdapter(new MusiqueAdaptateur(this, android.R.layout.simple_list_item_activated_1, controler.getGalerie().getMusiques()));
                }
                if(data.getBooleanExtra("SUPPR",false)) {
                    lfrag.resetPosition();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(playlist>=0) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.playlist_option_menu, menu);
        }
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_rename_playlist:
                View view =getLayoutInflater().inflate(R.layout.rename_playlist_popup, null);
                final EditText txt = view.findViewById(R.id.playlist_name);
                txt.setText(controler.getGalerie().getPlaylists().get(playlist).getName());
                Button btn = view.findViewById(R.id.rename_playlist_button);
                AlertDialog.Builder popupbuilder = new AlertDialog.Builder(this);
                final AlertDialog popup = popupbuilder.create();
                popup.setView(view);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(controler.getGalerie().renamePlaylist(controler.getGalerie().getPlaylists().get(playlist),txt.getText().toString())) {
                            getSupportActionBar().setTitle(controler.getGalerie().getPlaylists().get(playlist).getName());
                            popup.dismiss();
                        }
                    }
                });
                popup.show();
                return true;
            case R.id.action_suppr_playlist:
                controler.getGalerie().removePlaylist(playlist);
                Intent intent = getIntent();
                intent.putExtra("CONTROLER", controler);
                setResult(RESULT_OK, intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
