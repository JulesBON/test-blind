package com.example.blindtest.view.activities;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.MusiqueAdaptateur;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.modele.Musique;


public class ReadMusiquesActivity extends AppCompatActivity {

    private Controler controler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.view_load_music);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ListView list = findViewById(R.id.list);

        controler = (Controler)getIntent().getSerializableExtra("CONTROLER");

        controler.getLoader().loadAllAudioFromDevice(this);

       MusiqueAdaptateur adapter = new MusiqueAdaptateur(this,android.R.layout.simple_list_item_1, controler.getLoader().getLesMusiques());

        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                controler.getGalerie().addMusique((Musique)parent.getItemAtPosition(position));
            }
        });




        setResult(RESULT_OK,getIntent());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.load_music_menu, menu);
        return true;

    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load_all_musics:
                for (Musique music : controler.getLoader().getLesMusiques())
                    controler.getGalerie().addMusique(music);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }
}
