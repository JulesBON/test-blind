package com.example.blindtest.view.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.example.blindtest.R;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.modele.Musique;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class VuePrincipale extends AppCompatActivity {

    private Controler controler;
    ArrayList<Musique> list = new ArrayList<>();

    private static final int READ_STORAGE = 6;
    private static final int REQUEST_MUSIC = 1;
    private static int called = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vue_principale);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        File fichier = new File(getFilesDir()+"/save.bin");
        try{
            ObjectInputStream is = new ObjectInputStream(new FileInputStream(fichier));
            controler = (Controler) is.readObject();
        }
        catch(FileNotFoundException e){
            controler=new Controler(getString(R.string.name_galerie));
            return;
        }
        catch(IOException e){
            controler=new Controler(getString(R.string.name_galerie));
            return;
        }catch(ClassNotFoundException e){
            controler=new Controler(getString(R.string.name_galerie));
            return;
        }


    }

    @Override
    protected void onStop() {
        super.onStop();
        File fichier = new File(getFilesDir()+"/save.bin");
        try{
            FileOutputStream fo =new FileOutputStream(fichier);
            ObjectOutputStream os = new ObjectOutputStream(fo);
            os.writeObject(controler);
            os.close();
        }
        catch(FileNotFoundException e){
            return;
        }
        catch(IOException e){
            return;
        }
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putSerializable("CONTROLER", controler);
    }

    public void clickGalerie(View view) {
        Intent intent = new Intent(this, GalerieActivity.class);
        intent.putExtra("CONTROLER", controler);
        startActivityForResult(intent, REQUEST_MUSIC);
    }



    public void clickMusic (View view) {
        called = 1;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_STORAGE);
            }
        } else {
            Intent intent = new Intent(this, ReadMusiquesActivity.class);
            intent.setAction(Intent.ACTION_PICK);
            intent.putExtra("CONTROLER", controler);
            startActivityForResult(intent, REQUEST_MUSIC);

        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            if (requestCode == REQUEST_MUSIC) {
                controler = (Controler)data.getSerializableExtra("CONTROLER");
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        Intent intent;
        switch (requestCode) {
            case READ_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (called == 1) {
                        intent = new Intent(this, ReadMusiquesActivity.class);
                        intent.putExtra("CONTROLER", controler);
                        startActivityForResult(intent, REQUEST_MUSIC);
                    }
                    else {
                        intent = new Intent(this, GameActivity.class);
                        intent.putExtra("CONTROLER", controler);
                        startActivity(intent);
                    }
                } else {
                    Log.e("BOOO", "BOOO ACCEPTE");
                }
                return;
            }
        }
    }

    public void clickJouer(View view) {
        called = 2;
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_STORAGE);
            }
        } else {
            Intent intent = new Intent(this, SelectPlaylistActivity.class);
            intent.putExtra("CONTROLER", controler);
            startActivity(intent);

        }

    }

    public void parametre(View view) {
        Intent intent = new Intent(this, ParameterActivity.class);
        startActivity(intent);
    }

    public void clickQuit(View view) {
        finish();
    }

}


