package com.example.blindtest.modele;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PlayList implements Serializable {

    private String name;

    boolean selected;

    private List<Musique> musiques = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Musique> getMusiques() {
        return musiques;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public PlayList(String name){
        this.name = name;
    }

    public void addMusique(Musique musique) {
        if(!musiques.contains(musique))
            musiques.add(musique);
    }

    public void removeMusique(Musique musique){
        musiques.remove(musique);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayList playList = (PlayList) o;
        return name.equals(playList.name);
    }
}
