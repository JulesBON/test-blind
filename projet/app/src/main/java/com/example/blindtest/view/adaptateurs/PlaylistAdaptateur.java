package com.example.blindtest.view.adaptateurs;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.blindtest.modele.PlayList;

import java.util.List;

public class PlaylistAdaptateur extends ArrayAdapter<PlayList> {

    private boolean selectionnable;

    public PlaylistAdaptateur(@NonNull Context context, int resource, @NonNull List<PlayList> objects, boolean selectionnable) {
        super(context, resource, objects);
        this.selectionnable=selectionnable;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        final PlayList playList = getItem(position);
        LinearLayout layout = new LinearLayout(getContext());
        layout.setOrientation(LinearLayout.HORIZONTAL);
        if(selectionnable){
            CheckBox checkBox = new CheckBox(getContext());
            checkBox.setChecked(playList.isSelected());
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    playList.setSelected(isChecked);
                }
            });
            layout.addView(checkBox);
        }
        TextView name = new TextView(getContext());
        name.setText("Playlist: "+playList.getName());
        layout.addView(name);
        return layout;
    }
}
