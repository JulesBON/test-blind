package com.example.blindtest.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.blindtest.R;
import com.example.blindtest.view.adaptateurs.PlaylistAdaptateur;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.modele.PlayList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class ListPlaylistActivity extends AppCompatActivity {
    private Controler controler;
    ListView listPlaylists;

    private static final int REQUEST_MUSIC = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        controler = (Controler)getIntent().getSerializableExtra("CONTROLER");
        setContentView(R.layout.list_playlists);
        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        listPlaylists = findViewById(R.id.list_playlists);
        PlaylistAdaptateur adapter = new PlaylistAdaptateur(this,android.R.layout.simple_list_item_1, controler.getGalerie().getPlaylists(), false);

        listPlaylists.setAdapter(adapter);

        listPlaylists.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(getBaseContext(), MusiqueActivity.class);
                intent.putExtra("CONTROLER", controler);
                intent.putExtra("PLAYLIST", position);
                startActivityForResult(intent, REQUEST_MUSIC);
            }
        });

        setResult(RESULT_OK, getIntent());
    }

    @Override
    protected void onStop() {
        super.onStop();
        File fichier = new File(getFilesDir()+"/save.bin");
        try{
            FileOutputStream fo =new FileOutputStream(fichier);
            ObjectOutputStream os = new ObjectOutputStream(fo);
            os.writeObject(controler);
            os.close();
        }
        catch(FileNotFoundException e){
            return;
        }
        catch(IOException e){
            return;
        }
    }

    private void addPlaylist(){
        View view =getLayoutInflater().inflate(R.layout.add_playlist_popup, null);
        final EditText txt = view.findViewById(R.id.playlist_name);
        Button btn = view.findViewById(R.id.add_playlist_button);
        AlertDialog.Builder popupbuilder = new AlertDialog.Builder(this);
        final AlertDialog popup = popupbuilder.create();
        popup.setView(view);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controler.getGalerie().addPlaylist(new PlayList(txt.getText().toString()));
                ((PlaylistAdaptateur)listPlaylists.getAdapter()).notifyDataSetChanged();
                popup.dismiss();
            }
        });
        popup.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.playlists_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_playlist:
                addPlaylist();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            if (requestCode == REQUEST_MUSIC) {
                controler = (Controler)data.getSerializableExtra("CONTROLER");
                getIntent().putExtra("CONTROLER", controler);
                listPlaylists.setAdapter( new PlaylistAdaptateur(this,android.R.layout.simple_list_item_1, controler.getGalerie().getPlaylists(),false));
            }
        }
    }
}
