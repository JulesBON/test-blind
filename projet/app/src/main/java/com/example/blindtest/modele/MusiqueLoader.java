package com.example.blindtest.modele;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class MusiqueLoader {

    private List<Musique> lesMusiques = new ArrayList<>();

    public List<Musique> getLesMusiques() {
        return lesMusiques;
    }

    public void loadAllAudioFromDevice(final Context CONTEXT) {

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String[] projection = {MediaStore.Audio.AudioColumns.TITLE, MediaStore.Audio.AudioColumns.DATA};
        Cursor curs = CONTEXT.getContentResolver().query(uri,
                projection,
                null,
                null,
                null);

        if (curs != null) {
            while (curs.moveToNext()) {
                Musique musique = new Musique(curs.getString(0), curs.getString(1));

                lesMusiques.add(musique);
            }
            curs.close();
        }
    }
}
