package com.example.blindtest.view.activities;

import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Toolbar;

import androidx.fragment.app.FragmentActivity;

import com.example.blindtest.R;
import com.example.blindtest.view.fragments.DetailMusiqueFragment;

public class DetailMusiqueActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        if (savedInstanceState == null) {
            DetailMusiqueFragment details = new DetailMusiqueFragment();
            details.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, details).commit();
        }

    }

}
