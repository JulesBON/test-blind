package com.example.blindtest.view.activities;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.blindtest.R;
import com.example.blindtest.modele.Controler;
import com.example.blindtest.modele.Jeu;
import com.example.blindtest.modele.Musique;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class GameActivity extends AppCompatActivity {
    private final static String RAND_SONG = "array_rand";
    private final static String SCORE = "Score";
    private final static String PROG_SEEK = "Progress_seek";
    private final static String PROG_MUSIC = "Progress_music";
    private final static String ANSWER = "Answer";

    private Controler controler;
    private MediaPlayer music = new MediaPlayer();
    private Integer answer;
    private Random random = new Random();
    private List<Musique> chansons;
    private SeekBar seek;
    private LinearLayout linear;
    private TextView txtScore;
    private TextView txtNbBouton;
    private List<Integer>randSong;
    private Jeu jeu;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        controler = (Controler)getIntent().getSerializableExtra("CONTROLER");
        jeu = controler.getJeu();

        setContentView(R.layout.view_game);
        linear = findViewById(R.id.linearGame);
        seek = findViewById(R.id.seekNbButton);
        Button btnNext = findViewById(R.id.btnSetSong);
        txtScore = findViewById(R.id.textScore);
        txtNbBouton = findViewById(R.id.nbButton);

        seek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtNbBouton.setText("Vous aurez " + (seek.getProgress() + 2) + " boutons lors de la prochaine chanson");
                //Se referer à la ligne 66 pour savoir pourquoi (seek.getProgress()+2)

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
        });

        if(savedInstanceState != null){
            RecreateFromInstanceSaved(savedInstanceState);
            return;
        }
        /*
        Ici il faut seek.getProgress()+2 car il nous faut entre 2 et 8 boutons, or l'api minimum pour avoir l'attribut min pour la Seekbar est la 26 pour palier à ce probleme je defini la seekbar
        entre 0 et 6 mais en ajoutant +2, ce qui revient à être entre 2 et 8
         */
        txtScore.setText("Votre score est de : "+ jeu.getScore());
        chansons = jeu.getMusiques();
        txtNbBouton.setText("Vous aurez "+ (seek.getProgress()+2)+" boutons lors de la prochaine chanson");
        if(jeu.getMusiques().size()<seek.getMax()+2){
            seek.setMax(jeu.getMusiques().size()-2);
        }
        setSong();
    }

    private void RecreateFromInstanceSaved(Bundle bundle){
        Button btn;
        answer = bundle.getInt(ANSWER);
        randSong = bundle.getIntegerArrayList(RAND_SONG);
        seek.setProgress(bundle.getInt(PROG_SEEK));
        if(jeu.getMusiques().size()<seek.getMax()+2){
            seek.setMax(jeu.getMusiques().size()-2);
        }
        txtScore.setText("Votre score est de : "+ jeu.getScore());
        chansons = jeu.getMusiques();
        txtNbBouton.setText("Vous aurez "+ (seek.getProgress()+2)+" boutons lors de la prochaine chanson");
        //Se referer à la ligne 83 pour savoir pourquoi (seek.getProgress()+2)
        startSong(answer);
        Integer tmp = bundle.getInt(PROG_MUSIC);
        Log.d("JULES", tmp.toString());
        music.seekTo(bundle.getInt(PROG_MUSIC));
        music.start();
        for (int i = 0; i < randSong.size(); i++){
            btn = new Button(this);
            btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkAnswer(((Button) v));
                }
            });
            btn.setText(chansons.get(randSong.get(i)).getName());
            linear.addView(btn);
        }
    }

    private void setSong(){
        randSong = new ArrayList<>();
        int randBtn = random.nextInt(seek.getProgress()+2); //Se referer à la ligne 83 pour savoir pourquoi (seek.getProgress()+2);
        int randPos = random.nextInt(chansons.size());;
        Button btn;
        startSong(randPos);
        music.seekTo(chansons.get(randPos).getDebut());
        music.start();
        for (int i = 0; i < (seek.getProgress()+2); i++){ //Se referer à la ligne 83 pour savoir pourquoi (seek.getProgress()+2)
            btn = new Button(this);
            btn.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkAnswer(((Button) v));
                }
            });
            if (i == randBtn){
                randSong.add(randPos);
                answer = randPos;
                btn.setText(chansons.get(randPos).getName());
                Log.d("JULES", chansons.get(randSong.get(randBtn)).getName());
            }
            else {
                setRand(btn);
            }
            linear.addView(btn);
        }
    }


    private void startSong(int randPos){
        try {
            music.setDataSource(chansons.get(randPos).getPath());
            music.prepare();
        } catch (IOException e) {
            e.printStackTrace();

        }

    }

    private void checkAnswer(Button btn){
        CharSequence txt;
        if (btn.getText() == chansons.get(answer).getName()){
            jeu.addScore(1);
            txtScore.setText("Votre score est de : "+ jeu.getScore());
            music.pause();
            music.stop();
            music.reset();
            txt = "Bonne réponse c'etait bien " + btn.getText();
            Toast toast = Toast.makeText(this, txt, Toast.LENGTH_SHORT);
            toast.show();
            linear.removeAllViews();
            setSong();
        }
        else {
            txt = "Mauvaise réponse ce n'est pas " + btn.getText();
            Toast toast = Toast.makeText(this, txt, Toast.LENGTH_SHORT);
            toast.show();
        }

    }

    public void clickSong(View view) {
        music.pause();
        music.stop();
        music.reset();
        linear.removeAllViews();
        setSong();

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d("Ethan", "answer:"+answer);
        outState.putIntegerArrayList(RAND_SONG, (ArrayList<Integer>)randSong);
        outState.putInt(PROG_SEEK, seek.getProgress());
        Integer tmp = music.getCurrentPosition();
        outState.putInt(PROG_MUSIC, music.getCurrentPosition());
        outState.putInt(ANSWER, answer);
        super.onSaveInstanceState(outState);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        music.pause();
    }

    private void setRand(Button nomchanson){
        int randName;
        randName = random.nextInt(chansons.size());
        boolean contains = true;
        while(contains) {
            contains=false;
            for (int rand : randSong) {
                if (rand == randName) {
                    randName = random.nextInt(chansons.size());
                    contains=true;
                    break;
                }
            }
        }
        randSong.add(randName);
        Log.d("JULES", chansons.get(randSong.get(randSong.size()-1)).getName());
        nomchanson.setText(chansons.get(randSong.get(randSong.size()-1)).getName());

    }
}
